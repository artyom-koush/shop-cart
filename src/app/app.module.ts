import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductComponent } from './product-list/product/product.component';
import { CounterComponent } from './counter/counter.component';
import { ImageSelectorComponent } from './product-form/image-selector/image-selector.component';
import { ProductsService } from './shared/products.service';
import { ProductProfileComponent } from './product-profile/product-profile.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductFormComponent,
    ProductListComponent,
    ProductComponent,
    CounterComponent,
    ImageSelectorComponent,
    ProductProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
        { path: '', redirectTo: 'product-list', pathMatch: 'full' },
        { path: 'product-list', component: ProductListComponent },
        { path: 'product-profile/:id', component: ProductProfileComponent }
      ])
  ],
  providers: [
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
