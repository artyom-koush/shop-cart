import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../shared/product.model';
import { ProductsService } from '../shared/products.service';

@Component({
  selector: 'app-product-profile',
  templateUrl: './product-profile.component.html',
  styleUrls: ['./product-profile.component.scss']
})
export class ProductProfileComponent implements OnInit {

  public product: Product;
  public productTotalPrice: number;

  constructor(private route: ActivatedRoute, private pService: ProductsService) { }

  ngOnInit() {
    let productId: number = parseInt(this.route.snapshot.params['id']);
    this.product = this.pService.getProductById(productId);
    this.productTotalPrice = this.pService.getSingleProductTotalPrice(this.product);
  }

}
