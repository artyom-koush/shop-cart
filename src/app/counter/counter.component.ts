import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  private _currentAmount: number;

  @Input() set currentAmount(value: number) {
    this._currentAmount = value;
  }
  get currentAmount(): number {
    return this._currentAmount;
  }

  @Output() amountUpdated = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public increaseAmount() {
    this.currentAmount++;
    this.amountUpdated.emit(this.currentAmount);
  }

  public decreaseAmount() {
    if (this.currentAmount > 1) {
      this.currentAmount--;
      this.amountUpdated.emit(this.currentAmount);
    }
  }

}
