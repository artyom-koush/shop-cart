import { Component, OnInit } from '@angular/core';

import { ProductsService } from '../shared/products.service';
import { Product } from '../shared/product.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  public products: Product[];
  public productsTotalPrice: number;

  constructor(private pService: ProductsService) { }

  ngOnInit() {
    this.products = this.pService.getProducts();
    this.productsTotalPrice = this.pService.getProductsTotalPrice();
    this.pService.productsChanged
      .subscribe(
        (products: Product[]) => {
          this.products = products;
          this.productsTotalPrice = this.pService.getProductsTotalPrice();
        }
      );
  }

}
