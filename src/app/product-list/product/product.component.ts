import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../shared/product.model';
import { ProductsService } from '../../shared/products.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public productTotalPrice: number;

  @Input() product: Product;

  constructor(private pService: ProductsService) { }

  ngOnInit() {
    this.productTotalPrice = this.pService.getSingleProductTotalPrice(this.product);
  }

  public handleUpdatedAmount(newAmount: number) {
    this.pService.changeProductAmount(this.product.id, newAmount);
    this.productTotalPrice = this.pService.getSingleProductTotalPrice(this.product);
  }

  public deleteProduct() {
    this.pService.deleteProduct(this.product.id);
  }
}
