import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-image-selector',
  templateUrl: './image-selector.component.html',
  styleUrls: ['./image-selector.component.scss']
})
export class ImageSelectorComponent implements OnInit {

  private _selectedImage: string;

  @Input() set selectedImage(value: string) {
    this._selectedImage = value;
  }
  get selectedImage(): string {
    return this._selectedImage;
  }

  @Output() selectedImageUpdated = new EventEmitter;

  public imageSelectorShown: boolean;
  public productsImages: string[];

  constructor() {
    this.productsImages = [
      'apple',
      'carrot',
      'fish',
      'cupcake'
    ];
  }

  ngOnInit() {
    this.imageSelectorShown = false;
  }

  public toggleImageSelector() {
    this.imageSelectorShown = !this.imageSelectorShown;
  }

  public changeSelectedImage(image: string) {
    this.selectedImage = image;
    this.selectedImageUpdated.emit(this.selectedImage);
    this.imageSelectorShown = false;
  }

}
