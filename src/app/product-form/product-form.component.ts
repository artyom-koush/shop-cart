import { Component, OnInit, Input } from '@angular/core';

import { ProductsService } from '../shared/products.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {

  public productName: string;
  public productPrice: number;
  public productAmount: number;
  public productImage: string;

  constructor(private pService: ProductsService) { }

  ngOnInit() {
    this.setDefaultValues();
  }

  public handleUpdatedAmount(newAmount: number) {
    this.productAmount = newAmount;
  }

  public handleUpdatedSelectedImage(newImage: string) {
    this.productImage = newImage;
  }

  public addProduct() {
    if (!this.productName || !this.productPrice) {
      return;
    }

    this.pService.addProduct(
      this.productName,
      this.productPrice,
      this.productAmount,
      this.productImage
    );

    this.setDefaultValues();
  }

  private setDefaultValues() {
    this.productName = null;
    this.productPrice = null;
    this.productAmount = 1;
    this.productImage = 'cart';
  }

}
