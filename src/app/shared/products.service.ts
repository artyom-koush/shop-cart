import { EventEmitter } from '@angular/core';
import { Product } from './product.model';

export class ProductsService {

  private products: Product[] = [
    { id: 1, name: 'Item 1', price: 12, amount: 2, image: 'cart' },
    { id: 2, name: 'Item 2', price: 5, amount: 3, image: 'apple' }
  ];
  public productsChanged = new EventEmitter;

  public getProducts(): Product[] {
    return this.products;
  }

  public addProduct(name: string, price: number, count: number, image: string) {
    let newId = this.products[this.products.length - 1].id + 1;
    let product: Product = new Product(newId, name, price, count, image);
    this.products.push(product);
    this.productsChanged.emit(this.products);
  }

  public getProductsTotalPrice(): number {
    let totalPrice = 0;

    this.products.forEach((product: Product) => {
      totalPrice += product.amount * product.price;
    });

    return totalPrice;
  }

  public getSingleProductTotalPrice(product: Product): number {
    return product.amount * product.price;
  }

  public changeProductAmount(id: number, newAmount: number) {
    let productIndex = this.getProductIndex(id);
    this.products[productIndex].amount = newAmount;
    this.productsChanged.emit(this.products);
  }

  public deleteProduct(id: number) {
    let productIndex = this.getProductIndex(id);
    this.products.splice(productIndex, 1);
    this.productsChanged.emit(this.products);
  }

  public getProductById(id: number): Product {
    return this.products.find((product: Product): boolean => {
      return product.id === id;
    });
  }

  private getProductIndex(id: number): number {
    return this.products.findIndex((product: Product): boolean => {
      return id === product.id;
    });
  }

}
